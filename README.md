# Projeto Itau test

Esse projeto foi gerado com Angular [Angular CLI](https://github.com/angular/angular-cli) version 15.0.0.

Basicamente esse projeto exibe uma lista com jogadores das copas.

  Esse projeto tem 
* Lista paginada
* Graficos [Barra,Area,Pizza,Donuts]
* Pequena tabela dinamica com as informações dos jogadores
  
  Para o desenvolvimento foi utilizada a biblioteca [PLoty](https://plotly.com/) que esta consolidada no mercado,
  com muitas possivilidades de configurações e opçoes de graficos.

  Esse projeto tambem esta utilizando o bootstrap 4

## Development server

Para rodar esse projeto é necessario clonar esse repositorio e executar os seguintes comandos no terminal

- `npm install`
- `ng serve --open`

O servidor será iniciado em `http://localhost:4200/`. 

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.

## Running unit tests

Execute `ng test --code-coverage` os testes foram e gerados com [Karma](https://karma-runner.github.io).

![Cobertura de testes](converage.png)
