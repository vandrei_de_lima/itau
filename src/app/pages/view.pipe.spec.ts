import { Row } from './view/view.component';
import { ViewPipe } from './view.pipe';

describe('ViewPipe', () => {
  it('create an instance', () => {
    const pipe = new ViewPipe();
    expect(pipe).toBeTruthy();
  });

  it('should pipe retunr number 10',() => {
    const data = [{value:5},{value:5}] as Row[]
    const pipe = new ViewPipe();
    expect(pipe.transform(data)).toEqual(10)
  })
});
