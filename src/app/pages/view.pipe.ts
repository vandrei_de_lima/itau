import { Pipe, PipeTransform } from '@angular/core';
import { Row } from './view/view.component';

@Pipe({
  name: 'total',
})
export class ViewPipe implements PipeTransform {
  transform(rows: Row[]): number {
    let total = 0;

    rows.forEach((row) => {
      if (row.value) {
        total += row.value;
      }
    });

    return total;
  }
}
