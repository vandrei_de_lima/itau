import { ViewService } from './view.service';
import { MakeGraphService } from './../../../shared/services/graph/make-graph.service';
import { HttpClientModule } from '@angular/common/http';
import { ApiService } from './../../../shared/services/api/api.service';
import { fakeAsync, TestBed } from '@angular/core/testing';

import { of, take } from 'rxjs';

describe('ViewService', () => {
  let service: ViewService;
  let api: ApiService;
  let makeGraph: MakeGraphService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      providers: [ApiService, MakeGraphService],
    });
    service = TestBed.inject(ViewService);
    api = TestBed.get(ApiService);
    makeGraph = TestBed.get(MakeGraphService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should getResult retunr data', () => {
    service.player.next('teste');

    expect(service.getResult).toEqual('teste');
  });

  it('shoul setPageindex is called pageIndex is change', () => {
    service.setPageIndex(3);

    expect(service.pageIndex).toEqual(3);
  });

  it('should called api preparing of graph is started', async () => {
    const returnData: any = [
      {
        pace: '',
        shooting: '',
        passing: '',
        dribbling: '',
        defending: '',
        physicality: ' ',
      },
    ];
    spyOn(api, 'get').and.returnValue(of(returnData));
    service.preparePLayersData();

    const spy = spyOn(service, 'prepareGraphPlayers');
    service.preparePLayersData();

    take(300);

    expect(spy).toHaveBeenCalledWith(returnData);
  });
});
