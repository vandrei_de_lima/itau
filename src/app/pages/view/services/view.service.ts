import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ApiService } from 'src/app/shared/services/api/api.service';
import { MakeGraphService } from 'src/app/shared/services/graph/make-graph.service';

@Injectable({
  providedIn: 'root',
})
export class ViewService {
  player = new BehaviorSubject<any>([]);
  pageIndex: number = 1
  isLoading: boolean = false

  constructor(
    private api: ApiService,
    private graphService: MakeGraphService
  ) {}

  get getResult() {
    return this.player.value;
  }

  public preparePLayersData() {
    this.isLoading = true

    this.api.get(this.pageIndex).subscribe((players) => {
      this.prepareGraphPlayers(players);
    });
  }

  public prepareGraphPlayers(
    players: any[] = this.getResult,
    type: string = 'bar'
  ) {
    players.forEach((player: any) => {
      const { pace, shooting, passing, dribbling, defending, physicality } =
        player;

      const dataGrap: any = {
        values: [pace, shooting, passing, dribbling, defending, physicality],
        labels: ['Ritimo', 'Chute', 'Passe', 'Drible', 'Defesa', 'Fisico'],
      };

      player.id = `${player.id}`
      player.graph = this.graphService.make(dataGrap, type);
    });
    this.player.next(players);
    this.isLoading = false
  }

  public setPageIndex(index:number):void {
    this.pageIndex = index
  }
}
