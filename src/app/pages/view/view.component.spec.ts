import { ViewService } from './services/view.service';
import { ViewPipe } from './../view.pipe';
import { MakeGraphService } from './../../shared/services/graph/make-graph.service';
import { HttpClientModule } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewComponent } from './view.component';

describe('ViewComponent', () => {
  let component: ViewComponent;
  let fixture: ComponentFixture<ViewComponent>;
  let makeGraph:MakeGraphService
  let service: ViewService

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ViewComponent, ViewPipe],
      imports: [HttpClientModule, HttpClientModule],
      providers: [MakeGraphService, ViewService],
    }).compileComponents();

    fixture = TestBed.createComponent(ViewComponent);
    component = fixture.componentInstance;
    makeGraph = TestBed.get(MakeGraphService)
    service = TestBed.get(ViewService)
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call addNewRow is added new row in rows List', () => {
    component.addNewRow()

    expect(component.rows.length).toEqual(1)
  });

  it('should call changeType type is changes', () => {
    const spy =spyOn(service,'prepareGraphPlayers')
    component.changeType('type')


    expect(component.type).toEqual('type')
    expect(spy).toHaveBeenCalled()
  })

  it('should call changeRows updateDataClient is called', () => {
    spyOn(makeGraph,'updateDataClient')
    component.changeRows()

    expect(makeGraph.updateDataClient).toHaveBeenCalled()
  })
});
