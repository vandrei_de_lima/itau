import { Component, OnDestroy, OnInit } from '@angular/core';
import { MakeGraphService } from '../../shared/services/graph/make-graph.service';
import { Subject } from 'rxjs';
import { ViewService } from './services/view.service';

export type Row = {
  isEdit: boolean;
  name: string;
  value: number;
};
@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss'],
})
export class ViewComponent implements OnInit, OnDestroy {
  players = this.service.player;
  headers: string[] = ['Nome', 'Idade','Avaliação','Peso'];
  rows: Row[] = [];
  type: string = 'bar';

  private destroy$ = new Subject<void>();

  constructor(
    private makeGraph: MakeGraphService,
    public service: ViewService
  ) {}

  ngOnInit(): void {
    this.service.preparePLayersData();
  }

  addNewRow(): void {
    this.rows.push({ name: '', value: 0, isEdit: true });
  }

  changeType(type: string): void {
    this.type = type;
    this.service.prepareGraphPlayers(undefined, this.type);
  }

  changeRows(): void {
    this.makeGraph.updateDataClient(this.rows, this.type);
  }

  removeRow(index: number): void {
    this.players.value.splice(index, 1);
  }

  changePageIndex(pageIndex:number):void {
    this.service.setPageIndex(pageIndex)
    this.service.preparePLayersData()
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
