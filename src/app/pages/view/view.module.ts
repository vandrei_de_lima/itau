import { PlotlyDirective } from './../../shared/plotly/plotly.directive';
import { ViewComponent } from './view.component';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ViewRoutingModule } from './view-routing.module';
import { FormsModule } from '@angular/forms';
import { ViewPipe } from '../view.pipe';
import { ViewService } from './services/view.service';


@NgModule({
  declarations: [ViewComponent, ViewPipe],
  imports: [
    CommonModule,
    ViewRoutingModule,
    PlotlyDirective,
    FormsModule
  ],
  providers:[ViewService]
})
export class ViewModule { }
