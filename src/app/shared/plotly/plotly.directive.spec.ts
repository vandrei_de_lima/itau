import { TestBed } from '@angular/core/testing';
import { Component } from '@angular/core';
import { PlotlyDirective } from './plotly.directive';
import { By } from '@angular/platform-browser';

@Component({
  template: ` <div appPlotly id="myDiv" idGraph="myDiv" [data]="data"></div>`,
})
class TestComponent {
  data: any[] = [];
}

const trace1 = {
  values: [1, 2, 3],
  labels: ['a', 'b', 'c'],
  hole: 0,
  type: 'pie',
};
describe('PlotlyDirective', () => {
  let directive: PlotlyDirective;
  let fixture: any;
  let component: TestComponent;

  beforeEach(() => {
    fixture = TestBed.configureTestingModule({
      declarations: [TestComponent],
      imports: [PlotlyDirective],
    }).createComponent(TestComponent);

    component = fixture.componentInstance;
    fixture.detectChanges();
    component.data = component.data = [trace1];
    fixture.detectChanges();
    directive = fixture.debugElement.queryAll(By.directive(PlotlyDirective));
    window.dispatchEvent(new Event('resize'));
    fixture.detectChanges();
  });

  it('should create an instance', () => {
    expect(directive).toBeTruthy();
  });


});
