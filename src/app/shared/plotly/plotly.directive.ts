import {
  Directive,
  HostListener,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import * as Plotly from 'plotly.js';
@Directive({
  selector: '[appPlotly]',
  standalone: true,
})
export class PlotlyDirective implements OnChanges {
  @Input() data: any[] = [];
  @Input() layout: any = {
    margin: {
      l: 50,
      r: 50,
      b: 50,
      t: 50,
      pad: 4
    },
  };
  @Input() idGraph: string = 'graph';

  @HostListener('window:resize')
  public onResize() {
    Plotly.Plots.resize(this.idGraph);
  }

  ngOnChanges(changes: SimpleChanges): void {
    const { data } = changes;

    if (data) this.renderGraph();
  }

  renderGraph(): void {
    Plotly.newPlot(this.idGraph, this.data,this.layout);
  }
}
