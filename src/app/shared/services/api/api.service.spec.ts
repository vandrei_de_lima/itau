import { ApiService } from './api.service';
import { TestBed, inject } from '@angular/core/testing';
import {
  HttpClientTestingModule,
  HttpTestingController,
} from '@angular/common/http/testing';
import { HttpEvent, HttpEventType } from '@angular/common/http';

describe('ApiService', () => {
  let service: ApiService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [ApiService],
    });
    service = TestBed.inject(ApiService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should cleanObjectReturn retunr a objct', () => {
    const data: any = {
      id: '',
      name: '',
      age: '',
      rating: '',
      weight: '',
      pace: '',
      shooting: '',
      passing: '',
      dribbling: '',
      defending: '',
      physicality: '',
    };

    expect(service['cleanObjectReturn'](data)).toEqual(data);
  });

  it('should get users', inject(
    [HttpTestingController, ApiService],
    (httpMock: HttpTestingController, api: ApiService) => {
      const mockUsers = {
        items: [{teste:''}],
      };

      api.get(1).subscribe((event: HttpEvent<any>) => {
        switch (event.type) {
          case HttpEventType.Response:
            expect(event.body).toEqual(mockUsers);
        }
      });

      const mockReq = httpMock.expectOne(
        'https://futdb.app/api/players?page=1'
      );

      expect(mockReq.cancelled).toBeFalsy();
      expect(mockReq.request.responseType).toEqual('json');
      mockReq.flush(mockUsers);

      httpMock.verify();
    }
  ));
});
