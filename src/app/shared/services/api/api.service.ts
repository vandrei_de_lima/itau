import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { debounceTime, map, Observable, } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  constructor(private http: HttpClient) {}

  public get(pageIndex: number): Observable<any> {
    let headers = new HttpHeaders({
      'x-auth-token': 'e8432032-37e1-47ff-932d-9aba3b988082',
    });

    const url = `https://futdb.app/api/players?page=${pageIndex}`;


    return this.http.get<any>(url, { headers: headers }).pipe(
      map((result) =>
        result.items.map((item: any) => {

          return this.cleanObjectReturn(item)
        })
      ),
      debounceTime(300)
    );
  }

  private cleanObjectReturn(item:any):any {
    const {
      id,
      name,
      age,
      rating,
      weight,
      pace,
      shooting,
      passing,
      dribbling,
      defending,
      physicality,
    } = item;

    return {
      pace,
      shooting,
      passing,
      dribbling,
      defending,
      physicality,
      id,
      name,
      age,
      rating,
      weight,
    };
  }
}
