import { TestBed } from '@angular/core/testing';

import { MakeGraphService } from './make-graph.service';

describe('MakeGraphService', () => {
  let service: MakeGraphService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MakeGraphService],
    });
    service = TestBed.inject(MakeGraphService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should updateDataClient is called make function to haveCalled', () => {
    const spy = spyOn(service, 'make');
    service.updateDataClient([], '');

    expect(spy).toHaveBeenCalled();
  });

  it('should getBasicChart is called when make is called with type === bar', () => {
    const spy = spyOn(service, 'getBasicChart');
    service.make([]);
    expect(spy).toHaveBeenCalled();
  });

  it('should getCircularChart is called when make is called with type === pie', () => {
    const spy = spyOn(service, 'getCircularChart');
    service.make([], 'pie');
    expect(spy).toHaveBeenCalled();
  });

  it('should getBasicChart return a trace', () => {
    const data = { labels: ['teste'], values: [10] }

    const result:any = [
      {
        x: ['teste'],
        y: [10],
        fill: '',
        mode: 'lines+markers',
        type: 'bar',
      },
    ];

    const resultScatter:any = [
      {
        x: ['teste'],
        y: [10],
        fill: 'tozeroy',
        mode: 'lines+markers',
        type: 'scatter',
      },
    ];

    expect(service.getBasicChart(data,'scatter')).toEqual(resultScatter);
    expect(service.getBasicChart(data)).toEqual(result);
  });

  it('should getBasicChart return a trace', () => {
    const data = { labels: ['teste'], values: [10] }

    const result:any = [
      {
        labels: ['teste'],
        values: [10],
        hole: 0,
        type: 'pie',
      },
    ];

    const resultPizza:any = [
      {
        labels: ['teste'],
        values: [10],
        hole: 0.4,
        type: 'pie',
      },
    ];

    expect(service.getCircularChart(data)).toEqual(result);
    expect(service.getCircularChart(data,'donuts')).toEqual(resultPizza);
  });
});
