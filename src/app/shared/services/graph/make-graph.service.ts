import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

import { Graph } from '../../types/Graps.type';

type Trace = {
  x: any[];
  y: any[];
  fill: 'tozeroy' | '';
  mode: 'markers' | 'lines' | 'lines+markers';
  type: string;
};

@Injectable({
  providedIn: 'root',
})
export class MakeGraphService {
  data = new BehaviorSubject<any[]>([]);

  data$ = this.data.asObservable();

  constructor() {}

  public updateDataClient(rows: any, type: string): void {
    this.make(rows, type);
  }

  make(result: any, type: string = 'bar'): any {
    if (type === 'bar' || type === 'scatter') {
      return this.getBasicChart(result, type);
    } else {
      return this.getCircularChart(result, type);
    }
  }

  public getCircularChart(result: any, type: string = 'pie'): any[] {
    const trace1 = {
      values: result.values,
      labels: result.labels,
      hole: type !== 'pie' ? 0.4 : 0,
      type: 'pie',
    };

    return [trace1];
  }

  public getBasicChart(result: any, type: string = 'bar'): Trace[] {
    let data: any[] = [];

    const newData = [
      {
        x: result.labels,
        y: result.values,
        type: type,
      },
    ] as Graph[];

    newData.forEach((item: any) => {
      data.push(this.makeTrace(item));
    });

    return data;
  }

  private makeTrace(data: any): Trace {
    return {
      x: data.x,
      y: data.y,
      fill: this.getFill(data.type),
      mode: 'lines+markers',
      type: data.type,
    };
  }

  private getFill(type: string): 'tozeroy' | '' {
    return type === 'scatter' ? 'tozeroy' : '';
  }
}
