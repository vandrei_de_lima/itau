export type Client = {
  name: string;
  value: number;
  phone: string;
};
