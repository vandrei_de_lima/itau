export type Graph = {
  x: string[];
  y: number[];
  type: string;
};

export type Trace = { fill: string } & Graph;
